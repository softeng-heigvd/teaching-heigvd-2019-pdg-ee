# Teaching-HEIGVD-2019-PDG-EE

# Schedule

* **February 21st**: Kick-off, team composition, brainstorm: what do we want to build?
* **February 28th**: Presentation of the plan for Milestone 1
* **April 4th**: Demo of Milestone 1 + retro + presentation of the plan for Milestone 2
* **May 16th**: Demo of Milestone 2 + retro + presentation of the plan of final Milestone
* **June 13th**: Final demo + retro


# Planning

1. What do we want to achieve as a team?
- what are the scenarios that we want to demo (describe with narratives and/or mockups)
- what are the components that we want to build or integrate (architecture)
- what software engineering practices will we apply (testing, automation)

2. How is everyone in the team going to contribute?
- what will I focus on?
- what do I need to learn/do?
- what will I deliver?

3. What the risks?
- do we really know what we want to build and demo?
- what are technical unknowns?
- do we have well-balanced skills across the entire stack? (front-end, back-end, infra)
- can we work effectively as a team?
- time is a scarce resource: can we manage it effectively?